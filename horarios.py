#!/usr/bin/env python

#parte imports
import lxml
import pandas
from bs4 import BeautifulSoup
import bs4
import re
from tinydb import TinyDB, Query

#parte glossario
"""
group_schedule: horario de grupo. Todas las materias que se da en un grupo/salon/clase de la utp.
materia: una asignatura en un horario de grupo con un codigo de asignatura unico.
"""


#parte import the html into a pandas dataframe

def get_tables_from_html(file):
    """Return a list of the tables inside the html file."""
    return pandas.read_html(file)

#parte extract subject information from the pandas dataframe

def _convierte_pandas_list_a_lista_de_tuples(pandasThing):
   return zip(pandasThing._fields, pandasThing[:])

def _dias_en_una_hora(listaDeTuplesThing, subject):
    return [dia for dia, materia in listaDeTuplesThing if type(materia) == type("cadena") if subject in materia]

def _dia_orden(dia):
    dias = ["lunes",
            "martes",
            "miercoles",
            "jueves",
            "viernes",
            "sabado",
            "domingo"]
    return dias.index(dia.lower())

def subject_schedule(group_schedule, subject):
    """Retorna los [(dia, hora)...] en que se dicta la materia 'subject'.

    group_schedule es un dataframe, subject es una cadena.
    """

    horas_panda = group_schedule.itertuples()
    lista = []
    for hora in horas_panda:
        hoa = list(_convierte_pandas_list_a_lista_de_tuples(hora))[1:] # dropea el indice del dataframe
        tiempo = hoa[0][1] # guarda la hora en que se da la materia
        dias = _dias_en_una_hora(hoa, subject)
        if dias:
            for ds in dias:
               lista.append((ds, tiempo))
    lista.sort(key=lambda tup: _dia_orden(tup[0]))
    return lista

#subject info part

def subject_info(teachers_dataframe, subject):
    """Retorna un diccionario de la metadata de la materia 'subject'.

    metadata que retorna: codasig, codhora, profesor, email
    """
    subject_key = "ASIGNATURA"
    meta_panda = [i._asdict() for i in teachers_dataframe.itertuples()]
    for retorno in meta_panda:
        if subject in retorno[subject_key]:
            retorno.pop("Index")
            #retorno.pop(subject_key)
            return retorno

#BFS: So, there are two parts to a subject: its metadata, and its dictation times.
# These two are separated in the html, and these two last parts extract them.

#part extract all subjects data from dataframe list

def extract_subjects(teachers_dataframe):
    """Get a list of the subjects taught in this group."""
    return teachers_dataframe["ASIGNATURA"].to_list()

def subject_everything(teachers_dataframe, group_dataframe, subject):
    """Retorna el profesor, el grupo, y el horario de la materia 'subject' en el grupo 'group_dataframe'.

    subject es una cadena que hara submatch con la materia guardada en la tabla.
    group_dataframe es la lista de dataframes del grupo extraida del html.
    """

    retorno = subject_info(teachers_dataframe, subject)
    retorno["HORARIO"] = subject_schedule(group_dataframe, subject)
    return retorno

def extract_all_subjects(dataframeList):
    """Return a list of all subjects and their data taught in the class represented by the dataframe."""
    schedule_table, teachers_table = dataframeList
    subjects = extract_subjects(teachers_table)
    return [subject_everything(teachers_table, schedule_table, materia) for materia in subjects]
#BFS what is the dataframeList? Which function creates it? What do I compose this with?

#group metadata part

def parse_html_into_bs_class(html):
    """Turn a html file into bs4.BeautifulSoup object.

    this step should only be done once in the application.
    """
    with open(html) as ht:
        return bs4.BeautifulSoup(ht, "lxml")

def group_attributes_raw(group_html):
    """Get all the tags that have the attributes of the class that we want.

    group_html is a BeaturifulSoup object.
    """
    tag_identifier = re.compile("MainContent_[lL]bl")
    tag_attribute = "id"
    tag_name = "span"
    return group_html.find_all(tag_name, attrs={tag_attribute:tag_identifier})

def extract_from_tag(tag_html):
    """Get from the tag a dictionary with the attribute as key and the text as the val.

    tag_html is a bs4.element.Tag
    """
    tag_identifier = "MainContent_[lL]bl"
    rex_key_identifier = "(.+)"
    rex_key_extracter = tag_identifier + rex_key_identifier
    rex_key_extracter = re.compile(rex_key_extracter)
    target_string = tag_html.get("id")
    group_attribute = rex_key_extracter.findall(target_string)[0] # this should only return one match... possible error
    attribute_value = tag_html.get_text()
    return {group_attribute: attribute_value}

def group_metadata(group_html):
    """Return a dictionary of the metadata of the group the subject belongs to.

    group_html is a bs4.BeautifulSoup object.
    """
    retorno = {}
    for tag in group_attributes_raw(group_html):
        retorno.update(extract_from_tag(tag))
    return retorno

#html manipulation

def extract_all_from_html(file):
    """Return info on the subjects taught in the class represented by the html file."""
    subjects_in_group = extract_all_subjects(get_tables_from_html(file))
    metadata_of_group = group_metadata(parse_html_into_bs_class(file))
    for s in subjects_in_group:
        s.update(metadata_of_group)
    return subjects_in_group


def extract_all_from_folder(folder):
    import os
    files = [folder + "/" + fi for fi in os.listdir(folder) if ".html" in fi]
    lista = []
    for f in files:
        print("extracting from {}".format(f))
        lista += extract_all_from_html(f)
    return lista

#part horario choque

def chocan_las_materias(materia1, materia2):
    """Retorna True si las materias chocan, retorna False si coordinan.

    materiaN: diccionario de materia, refiere a #parte estructura
    """
    def entupla(lista_de_listas):
        return [tuple(i) for i in lista_de_listas]
    hor1, hor2 = entupla(materia1["HORARIO"]), entupla(materia2["HORARIO"])
    return not set(hor1).isdisjoint(hor2)

def agrega_a_horario_si_no_choca(horario, materia):
    """Retorna False si la materia choca con alguna de las materias en horario. Si no, horario + [materia].

    horario: lista de materias.
    materia: diccionario de materia, refiere a #parte estructura
    """
    for mat in horario:
        if chocan_las_materias(mat, materia):
            return horario
    return horario + [materia]

agregaHora = agrega_a_horario_si_no_choca

#database functions

# db = TinyDB("clasesDe<Facultad>.json")
# for i in extract_all_from_folder(folder):
#     db.insert(i)

materias = TinyDB("clasesSistemas.jpg")

def load_database_from_files(database, folder):
    """Add all subjects from all groups in all the files in the folder to the given database.

    A better name would have been 'load_all_subjects_into_database_from_folder. Yeah, long. No, needs no documentation.
    """
    for i in extract_all_from_folder(folder):
        database.insert(i)
    return database

def get_all_keys(facultad_database):
    retorna = set()
    for i in facultad_database:
        retorna = retorna.union(set(i.keys()))
    return list(retorna)
#BFS the names of the subjects are the keys of the database. So, you need all the keys to use the database
# to find any schedule conflict.

def collapse_values(facultad_database, field):
    retorna = set()
    for i in facultad_database:
        retorna.update([i[field]])
    return list(retorna)

def find_all_in_db(db, field, lista_de_matches):
    qry = Query()
    retorno = []
    for mat in lista_de_matches:
        enlace = db.search(qry[field] == mat)
        retorno += enlace
    return retorno
#BFS find all what? what is lista_de_matches? what is field?

def find_all_in_db_query(db, query, lista_de_matches):
    qry = Query()
    retorno = []
    for mat in lista_de_matches:
        enlace = db.search(query)
        retorno += enlace
    return retorno

def group_by(lista, funcion):
    valores_para_agrupar = set(map(funcion, lista))
    retorno = [[y
                for y in lista
                if funcion(y) == x]
               for x in valores_para_agrupar ]
    return retorno

def distribuye(ele, lista_de_listas):
    return [[ele] + i for i in lista_de_listas]

# arma_horarios([[mat1, mat11, mat111], [mat2, mat22, mat222], [mat3, mat33, mat333]])
def combina(lista_de_listas):
    if not lista_de_listas[1:]:
        return [[i] for i in lista_de_listas[0]]
    else:
        lst = []
        for i in lista_de_listas[0]:
            lst += distribuye(i, combina(lista_de_listas[1:]))
        return lst

def elimina_materia_que_no_encaja(horario_potencial):
    retorna = horario_potencial[:1]
    for i in horario_potencial[1:]:
        retorna = agrega_a_horario_si_no_choca(retorna, i)
    return retorna

def elimina_choques_de_horario(lista_de_potenciales_horarios):
    horarios_verificados = []
    for hora in lista_de_potenciales_horarios:
        lst = [hora[0]]
        for h in hora[1:]:
            lst = agrega_a_horario_si_no_choca(lst, h)
        horarios_verificados.append(lst)
    return horarios_verificados

def make_horarios_potenciales_with_materias(base_de_datos, clases):
    asig = find_all_in_db(materias, "ASIGNATURA", [facil_a_materias[i] for i in clases])
    materias_sin_choques = elimina_choques_de_horario(
        combina(
            group_by(asig, lambda x: x["ASIGNATURA"])))
    return materias_sin_choques

def agrupa_horarios_por_longitud(horarios_list):
    grupos = group_by(horarios_list, len)
    ordenado = {len(val[0]):val for val in grupos}
    return ordenado

#a = find_all_in_db(materias, "ASIGNATURA", [facil_a_materias[i] for i in ["fi", "hdpaa", "hdpab", "so", "sbeec"]])
def desde_folder_hasta_db(folder, las_materias, archivo_de_base_de_datos="auto.json"):
    basedatos = TinyDB(archivo_de_base_de_datos)
    asignaturas_de_materias = find_all_in_db(basedatos,
                                             "ASIGNATURA",
                                             las_materias)
    horarios_sin_choques = elimina_choques_de_horario(
        combina(
            group_by(asignaturas_de_materias,
                     lambda x: x["ASIGNATURA"])))

#a = find_all_in_db(materias, "ASIGNATURA", [facil_a_materias[i] for i in ["fi", "hdpaa", "hdpab", "so", "sbeec"]])
def dfhdb(folder, las_materias, archivo_de_base_de_datos="auto.json"):
    basedatos = TinyDB(archivo_de_base_de_datos)
    base_de_archivos = load_database_from_files(basedatos, folder)
    asignaturas_de_materias = find_all_in_db(base_de_archivos,
                                             "ASIGNATURA",
                                             las_materias)
    horarios_sin_choques = elimina_choques_de_horario(
        combina(
            group_by(asignaturas_de_materias,
                     lambda x: x["ASIGNATURA"])))
    return horarios_sin_choques
# make the database with all the grupos
#



# m = find_all_in_db(db, "ASIGNATURA", [facil_a_materias[i] for i in ["fi", "hdpaa", "hdpab", "so", "sbeec"]])
# group_by(m, lambda x: x["ASIGNATURA"])

#printing functions

def print_query(database, query):
    for i in database.search(query):
        print(i)

def print_horas_de_clase(horas):
    previo_dia = horas[0][0]
    for i in horas:
        if i[0] != previo_dia:
            previo_dia = i[0]
            print()
        print(i, end="\t")

def print_materia(materia):
    print(materia["ASIGNATURA"], materia["PROFESOR"], materia["Grupo"], materia["CODASIG"], ":", materia["CODHORA"])
    print_horas_de_clase(materia["HORARIO"])
    print("\n")

def print_horario(horario):
    for i in horario:
        print_materia(i)

def print_materias_potenciales(lista_de_horarios):
    for i in lista_de_horarios:
        print_horario(i)
        print("nuevo-grupo\n\n")

materias_a_facil = {
    'ANIMACIÓN DIG.': 'animacion digital y video juegos',
    'FÍSICA I (MEC.)': "fisica i",
    'BASE DE DATOS I': 'base de datos i',
    'TRABAJ.GRADUAC.': 'trabajo de graduacion',
    'SIST. OPERAT.': 'sistemas operativos',
    'CIRCUITOS LÓG.': 'circuitos logicos',
    'ESTAD. APOYO': 'estadistica con apoyo informatico',
    'EC. DIFE. ORD.': 'ecuaciones diferenciales ordinarias',
    'ESTRUCT.DATOS I': 'estructura de datos i',
    'ESTR. DISCR.': 'estructuras discretas para la computacion',
    'TÓP. ESP. II': 'topicos especiales ii',
    'GERENC.REC.H': 'gerencia de recursos humanos',
    'DIB. ASIST.COMP': 'dibujo asistido por computadora',
    'INGLÉS CONV.': 'ingles conversacional',
    'POL.LEG.INF.': 'politica y legislacion informatica',
    'MET. INV.ING.': 'metodologia de la investigacion',
    'TRAB.GRAD.I': 'trabajo de graduacion i',
    'CÁLCULO I': 'calculo i',
    'ING.AMBIENTAL': 'ingenieria ambiental',
    'HERR. PROGRAM.': 'herramientas de programacion aplicada a',
    'TECNOLOGÍA DE I': "tecnologia de informacion y comunicacion",
    'HERR.PROG.APL.': "herramientas de programacion aplicada b",
    'REDES INFORMÁT.': "redes informaticas",
    'INTELIG.ARTIFIC': "inteligencia artificial",
    'ING.SIST.DINAM.': "ingenieria de sistemas dinamicos",
    'SEG.TECN.COMP': "seguridad en tecnologia de computacion",
    'DES.LÓG. ALG.': "desarrollo logico y algoritmos",
    'REC. INF. EXP.O': "redaccion de informes y expresion oral",
    'INTERACC. HUM.': "interaccion humano computadora",
    'MECANICA': "mecanica",
    'ING. DE SOFTW.': "ingenieria de software",
    'SIST.BASAD.CONO': "sistemas basados en el conocimiento"
 }


#{'animacion digital y video juegos': 'ANIMACIÓN DIG.',
# 'fisica i': 'FÍSICA I (MEC.)',
# 'base de datos i': 'BASE DE DATOS I',
# 'trabajo de graduacion': 'TRABAJ.GRADUAC.',
# 'sistemas operativos': 'SIST. OPERAT.',
# 'circuitos logicos': 'CIRCUITOS LÓG.',
# 'estadistica con apoyo informatico': 'ESTAD. APOYO',
# 'ecuaciones diferenciales ordinarias': 'EC. DIFE. ORD.',
# 'estructura de datos i': 'ESTRUCT.DATOS I',
# 'estructuras discretas para la computacion': 'ESTR. DISCR.',
# 'topicos especiales ii': 'TÓP. ESP. II',
# 'gerencia de recursos humanos': 'GERENC.REC.H',
# 'dibujo asistido por computadora': 'DIB. ASIST.COMP',
# 'ingles conversacional': 'INGLÉS CONV.',
# 'politica y legislacion informatica': 'POL.LEG.INF.',
# 'metodologia de la investigacion': 'MET. INV.ING.',
# 'trabajo de graduacion i': 'TRAB.GRAD.I',
# 'calculo i': 'CÁLCULO I',
# 'ingenieria ambiental': 'ING.AMBIENTAL',
# 'herramientas de programacion aplicada a': 'HERR. PROGRAM.',
# 'tecnologia de informacion y comunicacion': 'TECNOLOGÍA DE I',
# 'herramientas de programacion aplicada b': 'HERR.PROG.APL.',
# 'redes informaticas': 'REDES INFORMÁT.',
# 'inteligencia artificial': 'INTELIG.ARTIFIC',
# 'ingenieria de sistemas dinamicos': 'ING.SIST.DINAM.',
# 'seguridad en tecnologia de computacion': 'SEG.TECN.COMP',
# 'desarrollo logico y algoritmos': 'DES.LÓG. ALG.',
# 'redaccion de informes y expresion oral': 'REC. INF. EXP.O',
# 'interaccion humano computadora': 'INTERACC. HUM.',
# 'mecanica': 'MECANICA',
# 'ingenieria de software': 'ING. DE SOFTW.',
# 'sistemas basados en el conocimiento': 'SIST.BASAD.CONO',
# 'adyvj': 'ANIMACIÓN DIG.',
# 'fi': 'FÍSICA I (MEC.)',
# 'bddi': 'BASE DE DATOS I',
# 'tdg': 'TRABAJ.GRADUAC.',
# 'so': 'SIST. OPERAT.',
# 'cl': 'CIRCUITOS LÓG.',
# 'ecai': 'ESTAD. APOYO',
# 'edo': 'EC. DIFE. ORD.',
# 'eddi': 'ESTRUCT.DATOS I',
# 'edplc': 'ESTR. DISCR.',
# 'tei': 'TÓP. ESP. II',
# 'gdrh': 'GERENC.REC.H',
# 'dapc': 'DIB. ASIST.COMP',
# 'ic': 'INGLÉS CONV.',
# 'pyli': 'POL.LEG.INF.',
# 'mdli': 'MET. INV.ING.',
# 'tdgi': 'TRAB.GRAD.I',
# 'ci': 'CÁLCULO I',
# 'ia': 'INTELIG.ARTIFIC',
# 'hdpaa': 'HERR. PROGRAM.',
# 'tdiyc': 'TECNOLOGÍA DE I',
# 'hdpab': 'HERR.PROG.APL.',
# 'ri': 'REDES INFORMÁT.',
# 'idsd': 'ING.SIST.DINAM.',
# 'setdc': 'SEG.TECN.COMP',
# 'dlya': 'DES.LÓG. ALG.',
# 'rdiyeo': 'REC. INF. EXP.O',
# 'ihc': 'INTERACC. HUM.',
# 'm': 'MECANICA',
# 'ids': 'ING. DE SOFTW.',
# 'sbeec': 'SIST.BASAD.CONO'}

facil_a_materias = {val:key for key, val in materias_a_facil.items()}
facil_a_materias.update({ "".join([j[0] for j in i.split()]): val for i, val in facil_a_materias.items()})


#play functions

def conteo_de_elementos(lista):
    dd = {}
    for i in lista:
        try:
            dd[i] += 1
        except KeyError:
            dd[i] = 1
    return dd

#parte estructura de la estructura de datos
#{'ASIGNATURA': 'TECNOLOGÍA DE I',
# 'CODASIG': 8353,
# 'CODHORA': 4253,
# 'PROFESOR': 'LOPEZ, VICTOR',
# 'EMAIL': 'victor.lopez@utp.ac.pa',
# 'HORARIO': [('MARTES', '9:30-10:15A.M.'), ('MARTES', '10:20-11:05A.M.'), ('JUEVES', '9:30-10:15A.M.'), ('JUEVES', '10:20-11:05A.M.'), ('JUEVES', '11:10-11:55A.M.')],
# 'Mensaje': 'Se dictará en Inglés el curso de Tecnología de la Información y Comunicación (TIC) en el grupo: 1IL311',
# 'Titulo': 'UNIVERSIDAD TECNOLÓGICA DE PANAMÁ',
# 'Facultad': 'FACULTAD DE INGENIERÍA DE SISTEMAS COMPUTACIONALES',
# 'Sede': 'SEDE PANAMÁ',
# 'TituloHorario': 'HORARIO DE CLASE',
# 'Grupo': '1IL311',
# 'Carrera': 'LIC.ING.SIST.Y COMP.',
# 'Nivel': 'I AÑO',
# 'Turno': 'DIURNO'}

# # tabla de horarios
# <table cellspacing="0" class="table table-hover table-responsive table-bordered" id="MainContent_gvHorario" style="border-collapse:collapse;">

# # tabla de profesores
# <table cellspacing="0" class="table table-hover table-responsive table-bordered" id="MainContent_Gvdetalle" style="border-collapse:collapse;">


#parte referencias
# https://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python
# compare two lists to see if they don't share elements, for chocan_horarios(lista_de_materias, nueva_materia)
# not set(a).isdisjoint(b) # even better results if a is smaller than b
# this answer is amazing, it analizes the runtime of 4 options for comparing lists, and makes plausible scenerarios to test it, with graphs!

# https://stackoverflow.com/questions/41256648/select-multiple-ranges-of-columns-in-pandas-dataframe
# df.iloc[:, np.r_[1:6]]
# also works for rows
